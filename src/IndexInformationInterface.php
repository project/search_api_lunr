<?php

namespace Drupal\search_api_lunr;

/**
 * Interface for the index information service.
 */
interface IndexInformationInterface {

  /**
   * Get the settings required to instantiate the API.
   *
   * @return array
   *   Settings passed into the JS API object.
   */
  public function getSettings(): array;

}
