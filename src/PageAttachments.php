<?php

namespace Drupal\search_api_lunr;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The PageAttachments class.
 */
class PageAttachments implements ContainerInjectionInterface {

  /**
   * The index information service.
   *
   * @var \Drupal\search_api_lunr\IndexInformationInterface
   */
  protected $indexInformation;

  /**
   * Create and instance of PageAttachments.
   */
  public function __construct(IndexInformationInterface $indexInformation) {
    $this->indexInformation = $indexInformation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('search_api_lunr.index_information')
    );
  }

  /**
   * Implements hook_page_attachments().
   */
  public function attachments(&$page) {
    $page['#attached']['drupalSettings']['search_api_lunr'] = $this->indexInformation->getSettings();
    $page['#attached']['library'][] = 'search_api_lunr/api';
  }

}
