<?php

namespace Drupal\search_api_lunr\Controller;

use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\search_api_lunr\IndexInformationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A controller for a a settings endpoint.
 */
class SettingsController extends ControllerBase {

  /**
   * The index information.
   *
   * @var \Drupal\search_api_lunr\IndexInformationInterface
   */
  protected $indexInformation;

  /**
   * SettingsController constructor.
   */
  public function __construct(IndexInformationInterface $indexInformation) {
    $this->indexInformation = $indexInformation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('search_api_lunr.index_information'));
  }

  /**
   * Return a JSON response for the index information.
   */
  public function build() {
    $response = new CacheableJsonResponse($this->indexInformation->getSettings());
    $response->setMaxAge(300);
    return $response;
  }

}
