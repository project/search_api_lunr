<?php

namespace Drupal\search_api_lunr\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * A controller for a search page.
 */
class SearchPage extends ControllerBase {

  /**
   * Return the controller for a search page.
   */
  public function build($server, $index) {
    return [
      '#theme' => 'search_api_lunr_page',
      '#server' => $server,
      '#index' => $index,
      '#attached' => [
        'library' => [
          'search_api_lunr/search',
        ],
      ],
    ];
  }

}
