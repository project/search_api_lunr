<?php

namespace Drupal\search_api_lunr\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\DataType\DataTypeInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Plugin\search_api\data_type\value\TextValueInterface;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_lunr\JsonContentIndex;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A search API back-end.
 *
 * @SearchApiBackend(
 *   id = "search_api_lunr",
 *   label = @Translation("JavaScript Back-end"),
 *   description = @Translation("Index items into a format readable by JS
 *   search engines.")
 * )
 */
class LunrBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait {
    submitConfigurationForm as traitSubmitConfigurationForm;
  }

  /**
   * The JSON index.
   *
   * @var \Drupal\search_api_lunr\JsonContentIndex
   */
  protected $jsonIndex;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->state = $container->get('state');
    $instance->fileSystem = $container->get('file_system');
    $instance->fileUrlGenerator = $container->get('file_url_generator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    $indexed = [];
    foreach ($items as $key => $item) {
      $item_data = [
        '_id' => $key,
      ];
      foreach ($item->getFields() as $field_name => $field) {
        $item_data[$field_name] = $this->processFieldValues($field->getDataTypePlugin(), $field->getValues());
      }
      $this->jsonIndex($index)->write($key, $item_data);
      $indexed[] = $key;
    }
    $this->bumpIndexVersion($index);
    return $indexed;
  }

  /**
   * Process field values.
   *
   * @param \Drupal\search_api\DataType\DataTypeInterface $dataType
   *   The data type.
   * @param array $values
   *   An array of values to process.
   *
   * @return array
   *   Processed values.
   */
  protected function processFieldValues(DataTypeInterface $dataType, array $values) {
    foreach ($values as &$value) {
      if ($value instanceof TextValueInterface) {
        $value = $value->getText();
      }
    }
    return implode(',', $values);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    foreach ($item_ids as $item_id) {
      $this->jsonIndex($index)->delete($item_id);
    }
    $this->bumpIndexVersion($index);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $this->jsonIndex($index)->clear();
    $this->bumpIndexVersion($index);
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
  }

  /**
   * Get a content index.
   *
   * @return \Drupal\search_api_lunr\JsonContentIndex
   *   A JSON content index.
   */
  protected function jsonIndex(IndexInterface $index) {
    if (!isset($this->jsonIndex[$this->getServer()->id()][$index->id()])) {
      $this->jsonIndex[$this->getServer()->id()][$index->id()] = new JsonContentIndex($this->getBasePath($index), $this->getIndexFileLimit(), $this->fileSystem);
    }
    return $this->jsonIndex[$this->getServer()->id()][$index->id()];
  }

  /**
   * Get the base path to the JS files.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return string
   *   The base path.
   */
  protected function getBasePath(IndexInterface $index) {
    return sprintf('public://search-api-js/%s/%s', $this->getServer()->id(), $index->id());
  }

  /**
   * Get the index file limit.
   *
   * @return int
   *   The number of files that the index should be split up between.
   */
  protected function getIndexFileLimit() {
    return $this->getConfiguration()['index_file_limit'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['index_file_limit'] = [
      '#title' => $this->t('Index file limit'),
      '#type' => 'number',
      '#min' => 1,
      '#max' => 50,
      '#description' => $this->t('The number of JSON files this index should be split up between. Writing to large files is memory intensive, so larger indexes will generally require a large file limit.'),
      '#default_value' => $this->getIndexFileLimit(),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->traitSubmitConfigurationForm($form, $form_state);
    foreach ($this->getServer()->getIndexes() as $index) {
      $index->clear();
    }
    $this->messenger()->addWarning($this->t('Server configuration saved, indexes will need to be reindexed.'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'index_file_limit' => 5,
    ];
  }

  /**
   * Get the current file list for the whole index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return array
   *   The file list.
   */
  public function getIndexFileList(IndexInterface $index) {
    $base_path = $this->getBasePath($index);
    $files = [];
    foreach (range(0, $this->getIndexFileLimit()) as $index_file) {
      $candidate_file = sprintf('%s/index-%d.json', $base_path, $index_file);
      if (file_exists($candidate_file)) {
        $files[] = $this->fileUrlGenerator->generateAbsoluteString($candidate_file);
      }
    }
    return $files;
  }

  /**
   * Get the index version.
   *
   * When a change is made to the index, it reindexed by clients.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index.
   *
   * @return int
   *   The index version.
   */
  public function getIndexVersion(IndexInterface $index) {
    $versions = $this->state->get('search_api_lunr_index_versions', []);
    return $versions[$index->id()] ?? 0;
  }

  /**
   * Bump the index version.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to bump.
   */
  protected function bumpIndexVersion(IndexInterface $index) {
    $versions = $this->state->get('search_api_lunr_index_versions', []);
    $versions[$index->id()] = $this->getIndexVersion($index) + 1;
    $this->state->set('search_api_lunr_index_versions', $versions);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsDataType($type) {
    return strpos($type, 'lunr') !== FALSE;
  }

}
