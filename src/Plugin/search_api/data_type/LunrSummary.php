<?php

namespace Drupal\search_api_lunr\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\TextDataType;

/**
 * Field type plugin.
 *
 * @SearchApiDataType(
 *   id = "search_api_lunr_summary",
 *   label = @Translation("Lunr: Summary source"),
 *   description = @Translation("Indicate to the Lunr API this field is the source for a summary."),
 * )
 */
class LunrSummary extends TextDataType {

  /**
   * {@inheritdoc}
   */
  public function getFallbackType() {
    return 'text';
  }

}
