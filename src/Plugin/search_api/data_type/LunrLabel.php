<?php

namespace Drupal\search_api_lunr\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\TextDataType;

/**
 * Field type plugin.
 *
 * @SearchApiDataType(
 *   id = "search_api_lunr_label",
 *   label = @Translation("Lunr: Label"),
 *   description = @Translation("Indicate to the Lunr API this field is a label."),
 * )
 */
class LunrLabel extends TextDataType {

  /**
   * {@inheritdoc}
   */
  public function getFallbackType() {
    return 'text';
  }

}
