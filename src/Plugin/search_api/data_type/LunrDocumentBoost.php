<?php

namespace Drupal\search_api_lunr\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\DecimalDataType;

/**
 * Field type plugin.
 *
 * @SearchApiDataType(
 *   id = "search_api_lunr_boost",
 *   label = @Translation("Lunr: Boost"),
 *   description = @Translation("Indicate to the Lunr API this field is an individual document boost."),
 * )
 */
class LunrDocumentBoost extends DecimalDataType {

  /**
   * {@inheritdoc}
   */
  public function getFallbackType() {
    return 'decimal';
  }

}
