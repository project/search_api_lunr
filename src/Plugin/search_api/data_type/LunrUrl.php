<?php

namespace Drupal\search_api_lunr\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\TextDataType;

/**
 * Field type plugin.
 *
 * @SearchApiDataType(
 *   id = "search_api_lunr_url",
 *   label = @Translation("Lunr: URL"),
 *   description = @Translation("Indicate to the Lunr API this field is a URL."),
 * )
 */
class LunrUrl extends TextDataType {

  /**
   * {@inheritdoc}
   */
  public function getFallbackType() {
    return 'text';
  }

}
