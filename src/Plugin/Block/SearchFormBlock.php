<?php

namespace Drupal\search_api_lunr\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Entity\SearchApiConfigEntityStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'User login' block.
 *
 * @Block(
 *   id = "search_api_lunr_form",
 *   admin_label = @Translation("Search form"),
 *   category = @Translation("Search API Lunr")
 * )
 */
class SearchFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The server config entity storage.
   *
   * @var \Drupal\search_api\Entity\SearchApiConfigEntityStorage
   */
  protected SearchApiConfigEntityStorage $serverStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->serverStorage = $container->get('entity_type.manager')->getStorage('search_api_server');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $index = Index::load($this->configuration['index']);
    $build = [
      '#theme' => 'search_api_lunr_form',
      '#server' => $index->getServerId(),
      '#index' => $index->id(),
      '#attached' => [
        'library' => [
          'search_api_lunr/search',
        ],
      ],
    ];
    if ($this->configuration['autocomplete_index']) {
      $autocomplete_index = Index::load($this->configuration['autocomplete_index']);
      $build['#autocomplete_index'] = $autocomplete_index->id();
      $build['#autocomplete_server'] = $autocomplete_index->getServerId();
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'index' => NULL,
      'autocomplete_index' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options = [];
    $servers = $this->serverStorage->loadByProperties([
      'backend' => 'search_api_lunr',
    ]);
    foreach ($servers as $server) {
      foreach ($server->getIndexes() as $index) {
        $options[$index->id()] = $index->label();
      }
    }

    $form['index'] = [
      '#title' => $this->t('Index'),
      '#description' => $this->t('Select the index the search block should submit searches to.'),
      '#required' => TRUE,
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getConfiguration()['index'],
    ];

    $form['autocomplete_index'] = [
      '#title' => $this->t('Autocomplete index'),
      '#description' => $this->t('If selected, this index will be searched against as an autocomplete query. This enables users to find results immediately without submitting the form.'),
      '#required' => FALSE,
      '#type' => 'select',
      '#empty_option' => $this->t('- Select -'),
      '#options' => $options,
      '#default_value' => $this->getConfiguration()['autocomplete_index'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['index'] = $form_state->getValue('index');
    $this->configuration['autocomplete_index'] = $form_state->getValue('autocomplete_index');
  }

}
