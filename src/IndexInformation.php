<?php

namespace Drupal\search_api_lunr;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Item\FieldInterface;

/**
 * The IndexInformation class.
 */
class IndexInformation implements IndexInformationInterface {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $serverStorage;

  /**
   * Create and instance of PageAttachments.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->serverStorage = $entityTypeManager->getStorage('search_api_server');
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    $settings = [
      'servers' => [],
    ];
    /** @var \Drupal\search_api\ServerInterface[] $servers */
    /** @var \Drupal\search_api_lunr\Plugin\search_api\backend\SearchApiJsBackend $backend */
    $servers = $this->serverStorage->loadByProperties([
      'backend' => 'search_api_lunr',
    ]);
    foreach ($servers as $server) {
      foreach ($server->getIndexes() as $index) {
        $backend = $server->getBackend();
        $settings['servers'][$server->id()]['indexes'][$index->id()]['fileList'] = $backend->getIndexFileList($index);
        $settings['servers'][$server->id()]['indexes'][$index->id()]['fields'] = array_map([$this, 'processField'], $index->getFields());
        $settings['servers'][$server->id()]['indexes'][$index->id()]['version'] = $backend->getIndexVersion($index);
      }
    }
    return $settings;
  }

  /**
   * Process a search API field into settings.
   *
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   The search API field.
   *
   * @return array
   *   An array of configuration.
   */
  public function processField(FieldInterface $field) {
    $build = [
      'boost' => $field->getBoost(),
    ];
    if (strpos($field->getDataTypePlugin()->getPluginId(), 'search_api_lunr_') === 0) {
      $build['special_type'] = substr($field->getDataTypePlugin()->getPluginId(), strlen('search_api_lunr_'));
    }
    return $build;
  }

}
