<?php

namespace Drupal\search_api_lunr;

use Drupal\Core\File\FileSystemInterface;

/**
 * Allow content to be written and read from a series of JSON files.
 */
class JsonContentIndex {

  /**
   * The base path to get data from.
   *
   * @var string
   */
  protected $basePath;

  /**
   * The number of files an index should be split into.
   *
   * @var int
   */
  protected $indexFiles;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * JsonContentIndex constructor.
   */
  public function __construct(string $basePath, int $indexFiles, FileSystemInterface $fileSystem) {
    $this->basePath = $basePath;
    $this->indexFiles = $indexFiles;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Write an item to the index.
   *
   * @param string $key
   *   The key to write for.
   * @param mixed $data
   *   Data to write.
   */
  public function write($key, $data) {
    $index = $this->readJson($key);
    $index[$key] = $data;
    $this->writeJson($key, $index);
  }

  /**
   * Delete an item from the index.
   *
   * @param string $key
   *   The key to delete a file for.
   */
  public function delete($key) {
    $data = $this->readJson($key);
    unset($data[$key]);
    $this->writeJson($key, $data);
  }

  /**
   * Clear the whole index.
   */
  public function clear() {
    $this->fileSystem->deleteRecursive($this->basePath);
  }

  /**
   * Read JSON from the index.
   *
   * @param string $key
   *   THe key to read.
   *
   * @return array
   *   The index data.
   */
  protected function readJson($key) {
    $filename = $this->getFileForKey($key);
    if (file_exists($filename)) {
      return json_decode(file_get_contents($filename), TRUE);
    }
    return [];
  }

  /**
   * Write JSON to the index.
   *
   * @param string $key
   *   THe key to write.
   * @param mixed $data
   *   The data.
   */
  protected function writeJson($key, $data) {
    $filename = $this->getFileForKey($key);
    $this->fileSystem->prepareDirectory($this->basePath, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    file_put_contents($filename, empty($data) ? '{}' : json_encode($data));
  }

  /**
   * Get the file for a given key.
   */
  protected function getFileForKey($key) {
    $numeric_hash = unpack('Ndata', md5($key, TRUE))['data'] % $this->indexFiles;
    return sprintf('%s/index-%d.json', $this->basePath, $numeric_hash);
  }

}
