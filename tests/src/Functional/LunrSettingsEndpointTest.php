<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\search_api_lunr\Kernel\LunrTestingTrait;

/**
 * @group search_api_lunr
 */
class LunrSettingsEndpointTest extends BrowserTestBase {

  use LunrTestingTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'user',
    'system',
    'search_api',
    'search_api_lunr',
    'jquery_ui_autocomplete',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createServerIndex();
  }

  /**
   * Test the lunr search.
   */
  public function testSettingsEndpoint(): void {
    $this->drupalGet('js-search/settings');
    $this->assertSession()->responseContains('{"servers":{"lunr":{"indexes":{"content":{"fileList":');
  }

}
