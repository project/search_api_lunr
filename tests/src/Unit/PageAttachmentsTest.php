<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\Unit;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\DataType\DataTypeInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\ServerInterface;
use Drupal\search_api_lunr\IndexInformation;
use Drupal\search_api_lunr\PageAttachments;
use Drupal\search_api_lunr\Plugin\search_api\backend\LunrBackend;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\search_api_lunr\PageAttachments
 * @group search_api_lunr
 */
class PageAttachmentsTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * @covers ::attachments
   */
  public function testPageAttachments(): void {
    $backend = $this->prophesize(LunrBackend::class);

    $dataType = $this->prophesize(DataTypeInterface::class);
    $dataType->getPluginId()->willReturn('search_api_lunr_label');

    $index = $this->prophesize(IndexInterface::class);
    $field = $this->prophesize(FieldInterface::class);
    $field->getBoost()->willReturn(1);
    $field->getDataTypePlugin()->willReturn($dataType->reveal());
    $index->getFields()->willReturn(['foo' => $field->reveal(), 'bar' => $field->reveal()]);
    $index->id()->willReturn('indexName');

    $backend->getIndexFileList($index)->willReturn([
      'foo.json',
      'foo.bar',
    ]);
    $backend->getIndexVersion($index)->willReturn(12);

    $server = $this->prophesize(ServerInterface::class);
    $server->getIndexes()->willReturn([
      $index->reveal(),
    ]);
    $server->getBackend()->willReturn($backend->reveal());
    $server->id()->willReturn('serverName');

    $serverStorage = $this->prophesize(ContentEntityStorageInterface::class);
    $serverStorage->loadByProperties(Argument::any())->willReturn([
      $server->reveal(),
    ]);

    $entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $entityTypeManager->getStorage(Argument::any())->willReturn($serverStorage->reveal());

    $indexInformation = new IndexInformation($entityTypeManager->reveal());

    $page = [];
    (new PageAttachments($indexInformation))->attachments($page);

    $this->assertEquals([
      'drupalSettings' => [
        'search_api_lunr' => [
          'servers' => [
            'serverName' => [
              'indexes' => [
                'indexName' => [
                  'fileList' => [
                    'foo.json',
                    'foo.bar',
                  ],
                  'fields' => [
                    'foo' => [
                      'boost' => 1,
                      'special_type' => 'label',
                    ],
                    'bar' => [
                      'boost' => 1,
                      'special_type' => 'label',
                    ],
                  ],
                  'version' => 12,
                ],
              ],
            ],
          ],
        ],
      ],
      'library' => [
        'search_api_lunr/api',
      ],
    ], $page['#attached']);
  }

}
