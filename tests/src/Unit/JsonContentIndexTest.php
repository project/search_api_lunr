<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\Unit;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Site\Settings;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\search_api_lunr\JsonContentIndex;
use Drupal\Tests\UnitTestCase;
use org\bovigo\vfs\vfsStream;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\search_api_lunr\JsonContentIndex
 * @group search_api_lunr
 */
class JsonContentIndexTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * Flex the index.
   */
  public function testWritingToIndex(): void {
    $fs = new FileSystem(
      $this->prophesize(StreamWrapperManagerInterface::class)->reveal(),
      new Settings([]),
    );

    vfsStream::setup('data');
    $index = new JsonContentIndex(vfsStream::url('data'), 3, $fs);
    $index->write('foo', [
      'bar' => 'baz',
    ]);
    $index->write('bar', [
      'bar' => 'baz',
    ]);
    $index->write('baz', [
      'bar' => 'baz',
    ]);
    $index->write('zap3', [
      'bar' => 'baz',
    ]);

    $this->assertTrue(\file_exists($this->indexFileUri(0)));
    $this->assertTrue(\file_exists($this->indexFileUri(1)));
    $this->assertTrue(\file_exists($this->indexFileUri(2)));

    $this->assertEquals('{"zap3":{"bar":"baz"}}', $this->readIndexFile(0));
    $this->assertEquals('{"foo":{"bar":"baz"}}', $this->readIndexFile(1));
    $this->assertEquals('{"bar":{"bar":"baz"},"baz":{"bar":"baz"}}', $this->readIndexFile(2));

    $index->write('zap3', [
      'flip' => 'blip',
    ]);
    $this->assertEquals('{"zap3":{"flip":"blip"}}', $this->readIndexFile(0));

    $index->delete('zap3');
    $this->assertEquals('{}', $this->readIndexFile(0));

    $index->clear();
    $this->assertFalse(\file_exists($this->indexFileUri(0)));
    $this->assertFalse(\file_exists($this->indexFileUri(1)));
    $this->assertFalse(\file_exists($this->indexFileUri(2)));
  }

  /**
   * Read an index file.
   */
  protected function readIndexFile($delta): string|bool {
    return \file_get_contents($this->indexFileUri($delta));
  }

  /**
   * Get an index file uri.
   */
  protected function indexFileUri($delta): string {
    return vfsStream::url('data') . "/index-$delta.json";
  }

}
