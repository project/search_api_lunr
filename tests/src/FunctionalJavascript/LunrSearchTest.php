<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;
use Drupal\search_api\IndexInterface;
use Drupal\search_api_lunr\Plugin\search_api\backend\LunrBackend;
use Drupal\Tests\search_api_lunr\Kernel\LunrTestingTrait;

/**
 * @group search_api_lunr
 */
class LunrSearchTest extends WebDriverTestBase {

  use LunrTestingTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';


  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'block',
    'user',
    'system',
    'search_api',
    'search_api_lunr',
    'jquery_ui_autocomplete',
  ];

  protected IndexInterface $index;
  protected LunrBackend $backend;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->index = $this->createServerIndex();
    $this->backend = $this->index->getServerInstance()->getBackend();
  }

  /**
   * Test the lunr search.
   */
  public function testLunrSearch(): void {
    $this->drupalPlaceBlock('search_api_lunr_form', [
      'index' => $this->index->id(),
      'autocomplete_index' => $this->index->id(),
    ]);

    Node::create([
      'title' => 'Foo',
      'type' => 'page',
      'uid' => 1,
    ])->save();
    Node::create([
      'title' => 'Bar',
      'type' => 'page',
      'uid' => 1,
    ])->save();
    $this->index->indexItems();

    $this->drupalGet('<front>');

    // Assert the UI autocomplete finds the result with a partial search.
    $session = $this->getSession();
    $assert = $this->assertSession();
    $session->getPage()->fillField('q', 'Fo');
    $session->getDriver()->keyDown($this->cssSelectToXpath('.ui-autocomplete-input'), 'o');
    $session->wait(1000, 'false');
    $assert->elementContains('css', '.ui-autocomplete', 'Foo');

    // Submitting the form will bring up full search results.
    $this->submitForm([
      'q' => 'Foo',
    ], 'Search');
    $session->wait(1000, 'false');
    $assert->linkExists('Foo');

    // Search is fuzzy based.
    $this->submitForm([
      'q' => 'baz',
    ], 'Search', 'search-page-form');
    $session->wait(1000, 'false');
    $assert->linkExists('Bar');

    // Searching for a term that doesn't exist will show a no results message.
    $this->submitForm([
      'q' => 'invalid',
    ], 'Search', 'search-page-form');
    $session->wait(1000, 'false');
    $assert->pageTextContains('No results found.');
  }

}
