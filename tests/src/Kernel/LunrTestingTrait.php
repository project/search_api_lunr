<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\Kernel;

use Drupal\node\Entity\NodeType;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Entity\Server;
use Drupal\search_api\IndexInterface;

/**
 * A testing trait.
 */
trait LunrTestingTrait {

  /**
   * Create a server and index.
   */
  protected function createServerIndex(): IndexInterface {
    NodeType::create([
      'type' => 'page',
      'label' => 'Page',
      'display_submitted' => FALSE,
    ])->save();

    $server = Server::create([
      'id' => 'lunr',
      'name' => 'Lunr',
      'description' => '',
      'backend' => 'search_api_lunr',
      'backend_config' => [
        'index_file_limit' => 5,
      ],
    ]);
    $server->save();

    $index = Index::create([
      'id' => 'content',
      'name' => 'Content',
      'description' => '',
      'read_only' => FALSE,
      'field_settings' => [
        'rendered_item' => [
          'label' => 'Rendered HTML output',
          'property_path' => 'rendered_item',
          'type' => 'search_api_lunr_summary',
          'configuration' => [
            'roles' => [
              'anonymous' => 'anonymous',
            ],
            'view_mode' => [
              'entity:node' => [
                'article' => 'default',
                'page' => 'default',
              ],
            ],
          ],
        ],
        'title' => [
          'label' => 'Title',
          'datasource_id' => 'entity:node',
          'property_path' => 'title',
          'type' => 'search_api_lunr_label',
          'boost' => 5.0,
          'dependencies' => [
            'module' => [
              'node',
            ],
          ],
        ],
        'url' => [
          'label' => 'URI',
          'property_path' => 'search_api_url',
          'type' => 'search_api_lunr_url',
          'boost' => 5.0,
        ],
      ],
      'datasource_settings' => [
        'entity:node' => [
          'bundles' => [
            'default' => TRUE,
            'selected' => [],
          ],
          'languages' => [
            'default' => TRUE,
            'selected' => [],
          ],
        ],
      ],
      'processor_settings' => [
        'add_url' => [],
        'aggregated_field' => [],
        'html_filter' => [
          'all_fields' => FALSE,
          'fields' => [
            'rendered_item',
          ],
          'title' => FALSE,
          'alt' => FALSE,
          'tags' => [],
          'weights' => [
            'preprocess_index' => -15,
            'preprocess_query' => -15,
          ],
        ],
        'language_with_fallback' => [],
        'rendered_item' => [],
      ],
      'tracker_settings' => [
        'default' => [
          'indexing_order' => 'fifo',
        ],
      ],
      'options' => [
        'index_directly' => TRUE,
        'cron_limit' => 50,
      ],
      'server' => 'lunr',
    ]);
    $index->save();

    return $index;
  }

}
