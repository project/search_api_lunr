<?php

declare(strict_types=1);

namespace Drupal\Tests\search_api_lunr\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\search_api\IndexInterface;
use Drupal\search_api_lunr\Plugin\search_api\backend\LunrBackend;
use Drupal\user\Entity\User;

/**
 * @group search_api_lunr
 * @coversDefaultClass \Drupal\search_api_lunr\Plugin\search_api\backend\LunrBackend
 */
class LunrIndexingTest extends KernelTestBase {

  use LunrTestingTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'user',
    'system',
    'search_api',
    'search_api_lunr',
  ];

  protected IndexInterface $index;
  protected LunrBackend $backend;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('search_api', ['search_api_item']);
    $this->installSchema('user', ['users_data']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('search_api_task');
    $this->installEntitySchema('node');
    $this->installEntitySchema('search_api_server');
    $this->installEntitySchema('search_api_index');
    $this->installConfig('search_api');
    $this->installConfig('system');

    User::create([
      'name' => 'foo',
    ])->save();

    $this->index = $this->createServerIndex();
    $this->backend = $this->index->getServerInstance()->getBackend();
  }

  /**
   * Test content indexing.
   */
  public function testIndexingVersionBump(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'page',
      'uid' => 1,
    ]);
    $node->save();
    $this->index->indexItems();
    $this->assertEquals(1, $this->backend->getIndexVersion($this->index));

    $node->title = 'Updated';
    $node->save();
    $this->index->indexItems();
    $this->assertEquals(2, $this->backend->getIndexVersion($this->index));

    $node->delete();
    $this->index->indexItems();
    $this->assertEquals(3, $this->backend->getIndexVersion($this->index));
  }

  /**
   * Test content indexing.
   */
  public function testIndexingFields(): void {
    $node = Node::create([
      'title' => 'Foo',
      'type' => 'page',
      'uid' => 1,
    ]);
    $node->save();
    $this->index->indexItems();

    $this->assertCount(1, $this->backend->getIndexFileList($this->index));
    $this->assertStringContainsString('/files/search-api-js/lunr/content/index-4.json', $this->backend->getIndexFileList($this->index)[0]);
    $this->assertEquals('{"entity:node\/1:en":{"_id":"entity:node\/1:en","rendered_item":"Foo","url":"\/node\/1","title":"Foo"}}', file_get_contents('public://search-api-js/lunr/content/index-4.json'));
  }

}
