import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from "rollup-plugin-terser";
import strip from '@rollup/plugin-strip';
import babel from 'rollup-plugin-babel';

let plugins = [
  resolve(),
  commonjs(),
  babel({
    exclude: 'node_modules/**' // only transpile our source code
  }),
];

if (process.env.BUILD === 'production') {
  plugins.push(strip());
  plugins.push(terser());
}

export default {
  input: ['src/api/api.index.js', 'src/search/search.index.js', 'src/theme/theme.index.js'],
  output: {
    dir: 'build',
    format: 'esm'
  },
  plugins: plugins
};
