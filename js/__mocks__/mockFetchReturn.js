function mockFetchReturn(fn, data) {
  fn.mockReturnValueOnce(Promise.resolve({
    json: () => Promise.resolve(data)
  }));
}
export default mockFetchReturn;
