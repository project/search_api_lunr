(function ({theme}) {

  /**
   * Overridable theme function when there are no results.
   */
  theme.searchApiLunrNoResults = Drupal.theme.searchApiLunrNoResults || function() {
    return '<p class="search-api-lunr-no-results">No results found.</p>';
  };

  /**
   * Overridable theme function for the search results summary.
   */
  theme.searchApiLunrResultSummary = Drupal.theme.searchApiLunrResultSummary || function(pagination) {
    let start = (pagination.pageNumber - 1) * pagination.pageSize + 1;
    let end = Math.min(pagination.pageNumber * pagination.pageSize, pagination.totalNumber);
    return `<p class="search-api-lunr-result-summary">Displaying ${start} - ${end} of ${pagination.totalNumber}</p>`;
  };

  /**
   * Overridable theme function for the search results.
   */
  theme.searchApiLunrResult = Drupal.theme.searchApiLunrResult || function(document) {
    let build = ['<div class="search-api-lunr-teaser">'];
    build.push(`<h2><a href="${document.getUrl()}">${document.getLabel()}</a></h2>`);
    let summary = document.getSummary();
    if (summary) {
      build.push(`<p>${summary}</p>`);
    }
    build.push('</div>');
    return build.join('');
  };

})(window.Drupal);
