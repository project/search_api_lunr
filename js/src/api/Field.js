/**
 * Represents a search API field.
 */
class Field {

  constructor(id, settings) {
    this.id = id;
    this.settings = settings;
  }

  /**
   * Get the ID of the field.
   *
   * @returns {string} The ID of the field
   */
  getId() {
    return this.id;
  }

  /**
   * Get the boost of the field.
   *
   * @returns {string} The boost of the field
   */
  getBoost() {
    return this.settings.boost;
  }

  getSpecialType() {
    return this.settings.special_type;
  }

}

export default Field;
