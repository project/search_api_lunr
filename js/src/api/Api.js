import Server from './Server';
import Index from './Index';

/**
 * An API singleton assigned to window.searchApiJs.
 */
class Api {

  constructor(settings) {
    this.servers = {};
    Object.keys(settings.servers).forEach((serverId) => {
      this.servers[serverId] = new Server(serverId, settings.servers[serverId]);
    });
  }

  /**
   * Get a list of server IDs that are available.
   *
   * @returns {string[]} A list of server IDs.
   */
  getServers() {
    return Object.keys(this.servers);
  }

  /**
   * Create an API object from a settings endpoint.
   *
   * @returns {Promise<Api>}
   */
  static createFromEndpoint(baseUrl) {
    return fetch(`${baseUrl}js-search/settings`)
      .then((data) => {
        return data.json();
      })
      .then((settings) => {
        return new Api(settings);
      });
  }

  /**
   * Get a JS representation of a Search API index.
   *
   * @param {string} serverId - The server ID
   * @returns {Server} The server
   */
  getServer(serverId) {
    if (!this.servers[serverId]) {
      throw new Error(`Did not have configuration for server with ID "${serverId}".`)
    }
    return this.servers[serverId];
  }

  /**
   * Test getting an index from an element.
   *
   * @param {HTMLOrForeignElement} element - An element to extract information from
   * @returns {Index} - The loaded index
   */
  getIndexFromElement(element) {
    return this.getServer(element.dataset.server).getIndex(element.dataset.index);
  }

}

export default Api;
