import localForage from 'localforage';
import lunr from 'lunr';
import Document from './Document';

/**
 * A lunr index, used to search through documents in a search API index.
 */
class LunrIndex {

  constructor(searchApiIndex) {
    this.searchApiIndex = searchApiIndex;
    this.hydratedIndexPromise = null;

    this.cachedIndex = localForage.createInstance({
      name: `lunr-index-${this.searchApiIndex.getId()}`,
    });
  }

  /**
   * Execute a search.
   *
   * @param {string} searchTerm - The term to search for
   * @param {int} maxResults - The number of results to collect
   */
  search(searchTerm, maxResults) {
    return this.hydrateIndex()
      .then((idx) => {
        const results = idx.query(function (q) {
          q.term(lunr.tokenizer(searchTerm), {boost: 100, usePipeline: true});
          q.term(lunr.tokenizer(searchTerm), {
            boost: 10,
            usePipeline: false,
            wildcard: lunr.Query.wildcard.TRAILING
          });
          q.term(lunr.tokenizer(searchTerm), {boost: 1, editDistance: 1});
        });
        return typeof maxResults !== 'undefined' ? results.slice(0, maxResults) : results;
      })
      .then((searchResults) => {
        return searchResults.map((searchResult) => this.searchApiIndex.getDocument(searchResult.ref));
      });
  }

  /**
   * Hydrate the lunr index.
   *
   * @returns {Promise<lunr, boolean>} A promise resolved with the hydrated
   *   index and if the index was loaded from cache or not
   */
  hydrateIndex() {
    if (this.hydratedIndexPromise) {
      return this.hydratedIndexPromise;
    }

    this.hydratedIndexPromise = new Promise((resolve, reject) => {
      // Make sure documents are loaded into the index first, so when search
      // results are returned, the index can be queried for the document.
      this.searchApiIndex.resolveAllDocuments().then((documents) => {

        console.log('Documents downloaded');

        this.cachedIndex.getItem('indexVersion').then((cachedIndexVersion) => {

          console.log('Downloaded index version: ' + this.searchApiIndex.getIndexVersion());
          console.log('Cached index version: ' + cachedIndexVersion);

          // If we have a cached version of the index that matches, load from
          // that index.
          if (cachedIndexVersion >= this.searchApiIndex.getIndexVersion()) {

            this.cachedIndex.getItem('index').then((cachedIndex) => {
              resolve(lunr.Index.load(JSON.parse(cachedIndex)));
              this._lastHydrateStrategy = 'cache';
              console.log('Loaded from cache');
            });

          } else {
            // Clear any potentially stale cached indexes and reindex from lunr.
            this.cachedIndex.clear();
            resolve(this.indexLunr(documents));
            this._lastHydrateStrategy = 'index';
            console.log('Reindexed from lunr');
          }
        });
      });
    });

    return this.hydratedIndexPromise;
  }

  indexLunr(documents) {
    let index = this.searchApiIndex;
    let lunrIndex = lunr(function () {
      this.ref('_id');

      // Inform lunr of the fields in the index, with boosting defined from
      // search API.
      index.getFields().map((field) => {
        // Skip indexing fields with a boost of zero.
        if (field.getBoost() !== 0) {
          this.field(field.getId(), {
            boost: field.getBoost(),
          });
        }
      });

      // Add each document in the index to the Lunr index.
      Object.keys(documents).map((documentId) => {
        // Check if a boost
        const document = index.getDocument(documentId);

        // Optionally add a boost to this specific document, if a boost field
        // has been defined in this index.
        const boost = document.getBoost();
        const attributes = boost ? {
          boost,
        } : {};

        this.add(documents[documentId], attributes);
      });

    });
    // Storing a string in indexDB seems to be much faster than storing
    // an object, potentially since objects can be searched for by their
    // properties. @todo, is there a way to natively store an object
    // but skip any kind of indexing process?
    this.cachedIndex.setItem('index', JSON.stringify(lunrIndex.toJSON()));
    this.cachedIndex.setItem('indexVersion', this.searchApiIndex.getIndexVersion());

    return lunrIndex;
  }

}

export default LunrIndex;
