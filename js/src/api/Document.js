/**
 * Represents a document from the search index.
 */
class Document {

  constructor(id, rawDocument, fields) {
    this.id = id;
    this.fields = fields;
    this.rawDocument = rawDocument;
  }

  getId() {
    return this.id;
  }

  getUrl() {
    return this.resolveSpecialFieldType('url', true);
  }

  getLabel() {
    return this.resolveSpecialFieldType('label', true);
  }

  getBoost() {
    return this.resolveSpecialFieldType('boost');
  }

  getSummary(searchTerm) {
    let summary = this.resolveSpecialFieldType('summary');
    if (summary && summary.length > 250) {
      summary = summary.substr(0, 250) + '...';
    }
    return summary ? summary : null;
  }

  /**
   * Resolve a special field type
   */
  resolveSpecialFieldType(type, errorOnFailure) {
    let field = this.fields.find((field) => field.getSpecialType() === type);
    if (field) {
      return this.rawDocument[field.getId()];
    }
    if (errorOnFailure) {
      throw new Error(`The index did not define a field with the type "${type}". Configure the fields in Search API and assign the Lunr data type for this field.`);
    }
    return null;
  }

}

export default Document;
