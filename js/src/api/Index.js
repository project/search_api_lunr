import LunrIndex from './LunrIndex';
import Document from './Document';
import Field from './Field';

/**
 * Represents a search API index.
 */
class Index {

  constructor(id, settings) {
    this.id = id;
    this.settings = settings;
    this.documentPromise = null;
    this.documentPromiseResolved = false;
    this.lunrIndex = new LunrIndex(this);
    this.fields = Object.keys(this.settings.fields).map((id) => new Field(id, this.settings.fields[id]));
  }

  /**
   * Resolve all documents from the file list.
   *
   * @returns {Promise<object[]>}
   */
  resolveAllDocuments() {
    if (this.documentPromise) {
      return this.documentPromise;
    }
    this.documentPromise = Promise.all(this.getFileList().map(url =>
      fetch(`${url}?version=${this.getIndexVersion()}`)
        .then((data) => {
          return data.json();
        })
    )).then((data) => {
      let documents = {};
      data.forEach((dataItem) => {
        documents = Object.assign(documents, dataItem);
      });
      this.documentPromiseResolved = true;
      this.documents = documents;
      return documents;
    });
    return this.documentPromise;
  }

  /**
   * Get the file list for this index.
   *
   * @returns {string[]} - A list of files
   */
  getFileList() {
    return this.settings.fileList;
  }

  /**
   * Get the ID of the index.
   *
   * @returns {string} The ID of the index
   */
  getId() {
    return this.id;
  }

  /**
   * Get a raw document from the index.
   *
   * @param {string} key The key of the document
   * @returns {object} The raw document object
   */
  getDocumentRaw(key) {
    if (!this.documentPromiseResolved) {
      throw new Error('Documents have not been resolved yet.');
    }
    if (!this.documents[key]) {
      throw new Error(`Index did not contain document with key "${key}".`);
    }
    return this.documents[key];
  }

  /**
   * Get a document from the index.
   *
   * @param {string} key - The key to search
   * @returns {Document} The document for the given key.
   */
  getDocument(key) {
    return new Document(key, this.getDocumentRaw(key), this.fields);
  }

  getFields() {
    return this.fields;
  }

  getIndexVersion() {
    return this.settings.version;
  }

  getLunr() {
    return this.lunrIndex;
  }

  /**
   * Execute a search.
   *
   * @param {string} searchTerm - The term to search for
   * @param {int} [maxResults] - The number of results to collect
   */
  search(searchTerm, maxResults) {
    return this.getLunr().search(searchTerm, maxResults);
  }

}

export default Index;
