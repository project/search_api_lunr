import Index from './Index';

/**
 * Represents a search API server.
 */
class Server {

  constructor(id, settings) {
    this.indexes = {};
    this.id = id;
    Object.keys(settings.indexes).forEach((indexId) => {
      this.indexes[indexId] = new Index(indexId, settings.indexes[indexId]);
    });
  }

  /**
   * Get an index from the server.
   *
   * @param {string} indexId - The ID of the index
   * @returns {Index} The index
   */
  getIndex(indexId) {
    if (!this.indexes[indexId]) {
      throw new Error(`Server did not contain and index with ID "${indexId}".`);
    }
    return this.indexes[indexId];
  }

  /**
   * Get the ID of the server.
   *
   * @returns {string} The ID of the server
   */
  getId() {
    return this.id;
  }

}

export default Server;
