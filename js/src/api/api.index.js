import Api from './Api';

(function() {
  // Initialize a property on window that can be used as an API for accessing
  // information about the indexes.
  window.searchApiJs = new Api(window.drupalSettings.search_api_lunr);
})();
