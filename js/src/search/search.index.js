import qs from 'qs';
import 'paginationjs';

(function ({debounce}, $, once, api) {

  /**
   * Implement lunr based searching on the index.
   */
  Drupal.behaviors.searchApiJsLunr = {
    attach: function (context, settings) {

      $(once('search-api-lunr-page', '.search-api-lunr-page', context)).each((i, element) => {
        const $container = $(element);
        const $search = $container.find('.search-api-js-page__input');
        const $form = $container.find('.search-api-js-page__form');
        const $results_container = $container.find('.search-api-js-page__results-container');
        const $results = $container.find('.search-api-js-page__results');
        const index = api.getIndexFromElement(element);

        const query = qs.parse(window.location.search, {ignoreQueryPrefix: true});
        if (query.q) {
          $search.val(query.q);
          executeSearchAndRender();
        }

        $form.on('submit', (e) => {
          e.preventDefault();
          executeSearchAndRender();
        });

        function executeSearchAndRender() {
          index.search($search.val().trim()).then((documents) => {
            $results_container.pagination({
              dataSource: documents,
              callback: (visibleDocuments, pagination) => {
                let content;

                if (visibleDocuments.length === 0) {
                  content = Drupal.theme('searchApiLunrNoResults');
                }
                else {
                  content = Drupal.theme('searchApiLunrResultSummary', pagination);
                  content += visibleDocuments.map((visibleDocument) => {
                    return Drupal.theme('searchApiLunrResult', visibleDocument);
                  }).join('');
                }

                $results.html(content);
              },
              className: 'search-api-lunr-pager',
              ulClassName: 'inline'
            });
          });
        }
      });

      $(once('search-api-lunr-autocomplete', '.search-api-lunr-autocomplete', context)).each((i, element) => {
        const index = api.getIndexFromElement(element);
        $(element).autocomplete({
          delay: 0,
          source: (request, response) => {
            index.search(request.term, 5).then((matchedDocuments) => {
              let autocompleteResponse = matchedDocuments.map((matchedDocument) => {
                return {
                  label: matchedDocument.getLabel(),
                  value: matchedDocument.getLabel(),
                  matchedDocument: matchedDocument,
                };
              });
              response(autocompleteResponse);
            });
          },
          select: (event, ui) => {
            window.location = ui.item.matchedDocument.getUrl();
          }
        });
      });
    }
  }

})(Drupal, jQuery, once, window.searchApiJs);
