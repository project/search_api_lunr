export default function(testBasicPagesIndexSchemaVersion) {
  return {
    "servers": {
      "site_content": {
        "indexes": {
          "basic_pages": {
            "fileList": [
              "/sites/default/files/search-api-js/lunr/content/index-0.json",
              "/sites/default/files/search-api-js/lunr/content/index-1.json",
              "/sites/default/files/search-api-js/lunr/content/index-2.json"
            ],
            "fields": {
              "rendered_item": {
                "boost": 1
              },
              "title": {
                "boost": 3
              },
              "url": {
                "boost": 0
              },
              "boost": {
                "special_type": "boost"
              }
            },
            "version": testBasicPagesIndexSchemaVersion || 122
          },
          "the_blog": {
            "fileList": [
              "/sites/default/files/search-api-js/lunr/content/index-0.json",
              "/sites/default/files/search-api-js/lunr/content/index-2.json",
              "/sites/default/files/search-api-js/lunr/content/index-4.json"
            ],
            "fields": {
              "title": {
                "boost": 1
              },
              "url": {
                "boost": 1
              },
              "boost": {
                "special_type": "boost"
              }
            },
            "version": 30
          }
        }
      }
    }
  }
}
