import Api from '../src/api/Api';
import sampleConfig from './__fixtures__/sampleConfig';
import mockFetchReturn from '../__mocks__/mockFetchReturn';

let api;
beforeEach(() => {
  api = new Api(sampleConfig());
});

test('getting a server that does not exist', () => {
  expect(() => api.getServer('foo')).toThrow(new Error('Did not have configuration for server with ID "foo".'))
});

test('getting a server', () => {
  expect(api.getServer('site_content').getId()).toEqual('site_content')
});

test('getting available servers', () => {
  expect(api.getServers()).toEqual(['site_content']);
});

test('getting an index from an element', () => {
  expect(api.getIndexFromElement({
    dataset: {
      server: 'site_content',
      index: 'the_blog',
    }
  }).getId()).toEqual('the_blog')
});

test('creating api from settings endpoint', (done) => {
  global.fetch = jest.fn();
  mockFetchReturn(global.fetch, {
    servers: {
      site_content: {indexes: {}}
    }
  });
  Api.createFromEndpoint('http://foo.com').then((api) => {
    expect(api.getServers()).toEqual(['site_content']);
    done();
  });
});
