import Api from "../src/api/Api";
import LunrIndex from "../src/api/LunrIndex";
import Index from "../src/api/Index";
import sampleConfig from './__fixtures__/sampleConfig';
import mockFetchReturn from '../__mocks__/mockFetchReturn';

import sampleDocumentsOne from './__fixtures__/documents-1';
import sampleDocumentsTwo from './__fixtures__/documents-2';
import sampleDocumentsThree from './__fixtures__/documents-3';

let lunr, index;

beforeEach((done) => {

  index = new Api(sampleConfig()).getServer('site_content').getIndex('basic_pages');
  global.fetch = jest.fn();
  // Mock three consecutive calls to fetch with some sample documents.
  mockFetchReturn(global.fetch, sampleDocumentsOne);
  mockFetchReturn(global.fetch, sampleDocumentsTwo);
  mockFetchReturn(global.fetch, sampleDocumentsThree);

  lunr = new LunrIndex(index);

  lunr.cachedIndex.clear().then(done);
});

test('searching for a single document', (done) => {
  // Only one document has the word "Abbas".
  lunr.search('Abbas', 5).then((results) => {
    expect(results.length).toEqual(1);
    expect(results[0].getId()).toEqual('entity:node/264:en');
  }).then(done);
});

test('searching for a multiple documents', (done) => {
  // Only one document has the word "Abbas".
  lunr.search('Vulpes', 5).then((results) => {
    expect(results.length).toEqual(2);
  }).then(done);
});

test('searching for a multiple documents with a limit', (done) => {
  // Only one document has the word "Abbas".
  lunr.search('Vulpes', 1).then((results) => {
    expect(results.length).toEqual(1);
  }).then(done);
});

test('searching for a multiple documents with no limit', (done) => {
  // Only one document has the word "Abbas".
  lunr.search('Vulpes').then((results) => {
    expect(results.length).toEqual(2);
  }).then(done);
});

test('test multiple searches hydrates index once', (done) => {
  // Only one document has the word "Abbas".
  let idx1, idx2;
  let searches = [
    lunr.hydrateIndex().then((idx) => {
      idx1 = idx;
    }),
    lunr.hydrateIndex().then((idx) => {
      idx2 = idx;
    }),
  ];
  Promise.all(searches).then(() => {
    expect(idx1 === idx2).toEqual(true);
  }).then(done);
});

test('loading a cached index', (done) => {
  // Execute a search which will hydrate a cached index.
  lunr.search('Vulpes', 1)
    .then((results) => {
      expect(results.length).toEqual(1);
      expect(lunr._lastHydrateStrategy).toEqual('index');
    })
    .then(() => {
      // By creating a second lunr index, we simulate a page load, in the
      // browser context, the persistent cache would load the index, but in
      // the test environment a second instantiation will read from the
      // memory index.
      let secondLunrIndex = new LunrIndex(index);
      secondLunrIndex.search('Vulpes', 1).then((results) => {
        expect(results.length).toEqual(1);
        expect(secondLunrIndex._lastHydrateStrategy).toEqual('cache');
      }).then(done);
    });
});



test('test smaller schema will not reindex', (done) => {
  // Execute a search which will hydrate a cached index.
  lunr.search('Vulpes', 1)
    .then((results) => {
      expect(results.length).toEqual(1);
      expect(lunr._lastHydrateStrategy).toEqual('index');
    })
    .then(() => {

      // Mock three additional fetch requests since the reinstantiation of the
      // API will redownload the documents.
      mockFetchReturn(global.fetch, sampleDocumentsOne);
      mockFetchReturn(global.fetch, sampleDocumentsTwo);
      mockFetchReturn(global.fetch, sampleDocumentsThree);

      // Create an instance of the API and index objects, with a smaller schema
      // version for the basic pages index.
      let oldIndex = new Api(sampleConfig(99)).getServer('site_content').getIndex('basic_pages');

      let oldLunrIndex = oldIndex.getLunr();
      oldLunrIndex.search('Vulpes', 1).then((results) => {
        expect(oldLunrIndex._lastHydrateStrategy).toEqual('cache');
        expect(results.length).toEqual(1);
      }).then(done);
    });
});

test('searching with field boosting', (done) => {
  global.fetch = jest.fn();
  mockFetchReturn(global.fetch, {
    "entity:node\/1015:en": {
      "_id": "entity:node\/1015:en",
      "title": "baz",
      "rendered_item": "foo",
      "url": "zip",
    },
    "entity:node\/1016:en": {
      "_id": "entity:node\/1016:en",
      "title": "foo",
      "rendered_item": "baz",
      "url": "zap",
    }
  });
  mockFetchReturn(global.fetch, {});
  mockFetchReturn(global.fetch, {});

  lunr.search('foo', 5).then((results) => {
    expect(results.length).toEqual(2);
    // 1016 should appear first, because title is a boosted field.
    expect(results[0].getId()).toEqual('entity:node/1016:en');
    expect(results[1].getId()).toEqual('entity:node/1015:en');
    done();
  });
});

test('searching with a zero field boost', (done) => {
  global.fetch = jest.fn();
  mockFetchReturn(global.fetch, {
    "entity:node\/1015:en": {
      "_id": "entity:node\/1015:en",
      "title": "baz",
      "rendered_item": "foo",
      "url": "zip",
    }
  });
  mockFetchReturn(global.fetch, {});
  mockFetchReturn(global.fetch, {});

  lunr.search('zip', 5).then((results) => {
    expect(results.length).toEqual(0);
    done();
  });
});

test('searching with document boosting', (done) => {
  global.fetch = jest.fn();
  mockFetchReturn(global.fetch, {
    "boosted 25": {
      "_id": "boosted 25",
      "title": "nulla",
      "boost": 25,
    },
    "boosted 35.5": {
      "_id": "boosted 35.5",
      "title": "nulla",
      "boost": '35.5',
    },
    "boosted 100": {
      "_id": "boosted 100",
      "title": "nulla",
      "boost": 100.00,
    },
    "boosted 1": {
      "_id": "boosted 1",
      "title": "nulla",
      "boost": 1,
    },
    "boosted 0.1": {
      "_id": "boosted 0.1",
      "title": "nulla",
      "boost": 0.1,
    },
  });
  mockFetchReturn(global.fetch, {});
  mockFetchReturn(global.fetch, {});

  lunr.search('nulla', 5).then((results) => {
    const order = results.map(result => result.getId());
    expect(order).toEqual([
      'boosted 100',
      'boosted 35.5',
      'boosted 25',
      'boosted 1',
      'boosted 0.1',
    ]);
    done();
  });
});
