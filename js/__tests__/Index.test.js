import Api from "../src/api/Api";
import sampleConfig from './__fixtures__/sampleConfig';
import mockFetchReturn from '../__mocks__/mockFetchReturn';

import sampleDocumentsOne from './__fixtures__/documents-1';
import sampleDocumentsTwo from './__fixtures__/documents-2';
import sampleDocumentsThree from './__fixtures__/documents-3';

let index;
beforeEach(() => {
  index = new Api(sampleConfig()).getServer('site_content').getIndex('basic_pages');
  global.fetch = jest.fn();
  // Mock three consecutive calls to fetch with some sample documents.
  mockFetchReturn(global.fetch, sampleDocumentsOne);
  mockFetchReturn(global.fetch, sampleDocumentsTwo);
  mockFetchReturn(global.fetch, sampleDocumentsThree);
});

test('resolving and managing documents', () => {
  expect(index.documentPromiseResolved).toEqual(false);

  index.resolveAllDocuments().then((data) => {
    // 7 documents should have been resolved in total.
    expect(Object.keys(data).length).toEqual(7);
    expect(data).toMatchSnapshot();
    expect(index.documentPromiseResolved).toEqual(true);
    // The fetch implementation should be called once for each item in the file
    // list.
    expect(global.fetch.mock.calls.length).toEqual(3);
  });
  // Resolving the documents again will not call fetch again.
  index.resolveAllDocuments().then((data) => {
    expect(Object.keys(data).length).toEqual(7);
    expect(global.fetch.mock.calls.length).toEqual(3);
  });
});

test('fetching a bad document from the index', () => {
  // Fetching a document from the index.
  index.resolveAllDocuments().then((data) => {
    expect(() => index.getDocumentRaw('not real')).toThrow(new Error('Index did not contain document with key "not real".'));
  });
});

test('fetching a document from the index', () => {
  // Fetching a document from the index.
  index.resolveAllDocuments().then((data) => {
    expect(index.getDocumentRaw('entity:node/1734:en')).toMatchSnapshot();
  });
});

test('getting the file list', () => {
  // The file list contains three documents.
  expect(index.getFileList()).toEqual([
    '/sites/default/files/search-api-js/lunr/content/index-0.json',
    '/sites/default/files/search-api-js/lunr/content/index-1.json',
    '/sites/default/files/search-api-js/lunr/content/index-2.json'
  ]);
});

test('getting the index version', () => {
  expect(index.getIndexVersion()).toEqual(122);
});

test('getting the id', () => {
  expect(index.getId()).toEqual('basic_pages');
});

test('getting the fields', () => {
  expect(index.getFields()).toHaveLength(4);
});

test('searching method proxies to lunr class', (done) => {
  index.search('not real search', 1).then((results) => {
    expect(results).toEqual([]);
    done();
  });
});
