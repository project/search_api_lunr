import Api from "../src/api/Api";
import sampleConfig from './__fixtures__/sampleConfig';

let fields;
beforeEach(() => {
  fields = new Api(sampleConfig()).getServer('site_content').getIndex('basic_pages').getFields();
});

test('field getters', () => {
  expect(fields[0].getId()).toEqual('rendered_item');
  expect(fields[0].getBoost()).toEqual(1);
});
