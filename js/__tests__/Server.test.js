import Api from '../src/api/Api';
import sampleConfig from './__fixtures__/sampleConfig';

let server;
beforeEach(() => {
  server = new Api(sampleConfig()).getServer('site_content');
});

test('getting the id', () => {
  expect(server.getId()).toEqual('site_content');
});

test('getting a bad index', () => {
  expect(() => server.getIndex('foo')).toThrow(new Error('Server did not contain and index with ID "foo".'))
});

test('getting an index', () => {
  expect(server.getIndex('basic_pages').getId()).toEqual('basic_pages');
});
