import Document from '../src/api/Document';
import Field from "../src/api/Field";

window.Drupal = {
  theme: {},
};
require('../src/theme/theme.index');

const searchApiLunrResult = window.Drupal.theme.searchApiLunrResult;
const fields = [
  new Field('rendered_item', {
    special_type: 'summary',
  }),
  new Field('url', {
    special_type: 'url',
  }),
  new Field('title', {
    special_type: 'label',
  }),
];

test('rendering a teaser with no info', () => {
  expect(() => {
    searchApiLunrResult(new Document('foo', {}, []))
  }
  ).toThrow(new Error('The index did not define a field with the type "url". Configure the fields in Search API and assign the Lunr data type for this field.'));
});

test('rendering a teaser with a url and title', () => {
  expect(searchApiLunrResult(new Document('foo', {
    url: '/foo',
    title: 'Foo'
  }, fields))).toMatchSnapshot();
});

test('rendering a teaser with a url, title and rendered item', () => {
  expect(searchApiLunrResult(new Document('foo', {
    url: '/foo',
    title: 'Foo',
    rendered_item: 'Sample content rendered...',
  }, fields))).toMatchSnapshot();
});

test('rendering error, no label', () => {
  expect(() => {
    searchApiLunrResult(new Document('foo', {
      url: '/foo',
      title: 'Foo'
    }, [
      new Field('url', {
        special_type: 'url',
      }),
    ]))
  }).toThrow(new Error('The index did not define a field with the type "label". Configure the fields in Search API and assign the Lunr data type for this field.'));
});

test('rendering error, no url', () => {
  expect(() => {
    searchApiLunrResult(new Document('foo', {
      url: '/foo',
      title: 'Foo'
    }, [
      new Field('title', {
        special_type: 'label',
      }),
    ]))
  }).toThrow(new Error('The index did not define a field with the type "url". Configure the fields in Search API and assign the Lunr data type for this field.'));
});
