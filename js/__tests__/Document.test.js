import Document from '../src/api/Document';
import Field from '../src/api/Field';

let document;

beforeEach(() => {

  const fields = [
    new Field('rendered_item', {
      special_type: 'summary',
    }),
    new Field('url', {
      special_type: 'url',
    }),
    new Field('title', {
      special_type: 'label',
    }),
  ];

  document = new Document('entity:node/1734:en', {
    _id: 'entity:node/1734:en',
    rendered_item: 'Iriure Loquor Nulla Paulatim.',
    url: '/node/1734',
    title: 'Iriure Loquor Nulla Paulatim'
  }, fields);
});

test('getting the id', () => {
  expect(document.getId()).toEqual('entity:node/1734:en');
});

test('getting the URL', () => {
  expect(document.getUrl()).toEqual('/node/1734');
});

test('getting the label', () => {
  expect(document.getLabel()).toEqual('Iriure Loquor Nulla Paulatim');
});

test('getting the summary', () => {
  expect(document.getSummary('Nulla')).toEqual('Iriure Loquor Nulla Paulatim.');
});

test('errors when no fields are defined', () => {
  let errorDoc = new Document('entity:node/1734:en', {
    _id: 'entity:node/1734:en',
    rendered_item: 'Iriure Loquor Nulla Paulatim.',
    url: '/node/1734',
    title: 'Iriure Loquor Nulla Paulatim'
  }, []);
  expect(() => errorDoc.getUrl()).toThrow(new Error('The index did not define a field with the type "url". Configure the fields in Search API and assign the Lunr data type for this field.'));
  expect(() => errorDoc.getLabel()).toThrow(new Error('The index did not define a field with the type "label". Configure the fields in Search API and assign the Lunr data type for this field.'));
  expect(errorDoc.getSummary('foo')).toEqual(null);
  expect(errorDoc.getBoost('foo')).toEqual(null);
});
